# SurroundSound

Assets:
- [Standard Assets (for Unity 2017.3)](https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2017-3-32351)
- [FREE Stylized PBR Textures Pack](https://assetstore.unity.com/packages/2d/textures-materials/free-stylized-pbr-textures-pack-111778)
- [Simple Modular Street Kit](https://assetstore.unity.com/packages/3d/environments/urban/simple-modular-street-kit-13811)
- [Simple City pack plain](https://assetstore.unity.com/packages/3d/environments/urban/simple-city-pack-plain-100348)
- [Modular self-stand fence](https://assetstore.unity.com/packages/3d/props/modular-self-stand-fence-105862)
- [Realistic Tree 9 Rainbow Tree](https://assetstore.unity.com/packages/3d/vegetation/trees/realistic-tree-9-rainbow-tree-54622)
- [Meshtint Free Chick Mega Toon Series](https://assetstore.unity.com/packages/3d/characters/animals/meshtint-free-chick-mega-toon-series-152777)
- [Simple Cars Pack](https://assetstore.unity.com/packages/3d/vehicles/land/simple-cars-pack-97669)
- [Tarbo - City 'Traffic Lights' Pack FREE](https://assetstore.unity.com/packages/3d/environments/urban/tarbo-city-traffic-lights-pack-free-154053)
- [Galactic Heroes Cartoon Spaceship](https://assetstore.unity.com/packages/3d/galactic-heroes-cartoon-spaceship-70188)


Sounds (freesound.org):
- [Footstep Grass 2 by GiocoSound](https://freesound.org/people/GiocoSound/sounds/421130/)
- [Footstep Dirt by LittleRobotSoundFactory](https://freesound.org/people/LittleRobotSoundFactory/sounds/270419/)
- [Footsteps Tile by DWOBoyle](https://freesound.org/people/DWOBoyle/sounds/458339/)
- [Birds by derjuli](https://freesound.org/people/derjuli/sounds/275962/)
- [Spring Birds Forest by ANARKYA](https://freesound.org/people/ANARKYA/sounds/387425/)
- [police siren by vlammenos](https://freesound.org/people/vlammenos/sounds/52906/)
- [car horn by krissyeliot](https://freesound.org/people/krissyeliot/sounds/125520/)
- [Chicken clucking by Breviceps](https://freesound.org/people/Breviceps/sounds/456803/)
- [Buzzing old fridge by draftcraft](https://freesound.org/people/draftcraft/sounds/434344/)
- [Spaceship Cruising, UFO by BurghRecords](https://freesound.org/people/BurghRecords/sounds/415673/)

All sounds were normalized.


Windows Build is in folder Projekt_3.