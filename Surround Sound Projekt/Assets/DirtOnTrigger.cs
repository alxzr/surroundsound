﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class DirtOnTrigger : MonoBehaviour
{
    public GameObject fps;
    private FirstPersonController script;

    // Start is called before the first frame update
    void Start()
    {
        script = fps.GetComponent<FirstPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "FPSController") {
            script.step = "Play_step_dirt";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "FPSController")
        {
            script.step = "Play_step_grass";
        }
    }
}
